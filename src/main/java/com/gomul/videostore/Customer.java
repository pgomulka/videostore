package com.gomul.videostore;

import java.util.ArrayList;
import java.util.List;

public class Customer {
	private String name;
	//todo this might be separate class for list of rentals 
	private List<Rental> rentals = new ArrayList<Rental>();

	public Customer(String name) {
		this.name = name;
	}

	public void addRental(Rental rental) {
		rentals.add(rental);
	}

	public String getName() {
		return name;
	}

	public String statement() {
		double totalAmount = 0;
		int frequentRenterPoints = 0;
		
		StringBuilder result = new StringBuilder();
		result.append(createHeader());

		for (Rental rental : this.rentals) {
			frequentRenterPoints += rental.getFrequentRentalPoints();
			result.append( rental.createSummary());
			totalAmount += rental.getFee();
		}

		result.append( createFeeSummary(totalAmount));
		result.append( createFrequentRenterSummary(frequentRenterPoints));

		return result.toString();
	}

	private String createFrequentRenterSummary(int frequentRenterPoints) {
		return "You earned " + String.valueOf(frequentRenterPoints) + " frequent renter points\n";
	}

	private String createFeeSummary(double totalAmount) {
		return "You owed " + String.valueOf(totalAmount) + "\n";
	}

	private String createHeader() {
		return "Rental Record for " + getName() + "\n";
	}

}