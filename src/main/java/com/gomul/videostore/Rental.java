package com.gomul.videostore;

public class Rental {
	private Movie movie;
	private int daysRented;

	public Rental(Movie movie, int daysRented) {
		this.movie = movie;
		this.daysRented = daysRented;
	}

	public int getDaysRented() {
		return daysRented;
	}

	public Movie getMovie() {
		return movie;
	}
	
	public int getFrequentRentalPoints() {
		return movie.getFrequentRentalPoints(daysRented);
	}
	
	public double getFee() {
		return movie.getFeeFor(daysRented);
	}
	
	public String createSummary() {
		return "\t" + getMovie().getTitle() + "\t" + getFee() + "\n";
	}
}