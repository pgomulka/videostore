package com.gomul.videostore;

//todo consider enum
public interface MovieType {
	double calculateFee(int daysRented);
	int getFrequentRenterPoints(int daysRented);
}
