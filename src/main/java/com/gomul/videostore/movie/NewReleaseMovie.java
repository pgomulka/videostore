package com.gomul.videostore.movie;

import com.gomul.videostore.MovieType;

public class NewReleaseMovie implements MovieType {

	public double calculateFee(int daysRented) {
		return daysRented * 3;
	}

	public int getFrequentRenterPoints(int daysRented) {
		if (daysRented > 1)
			return 2;
		return 1;
	}
}
