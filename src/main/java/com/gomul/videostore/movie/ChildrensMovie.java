package com.gomul.videostore.movie;

import com.gomul.videostore.MovieType;

public class ChildrensMovie implements MovieType {

	public double calculateFee(int daysRented) {
		double thisAmount = 1.5;
		if (daysRented > 3)
			thisAmount += (daysRented - 3) * 1.5;
		return thisAmount;
	}

	public int getFrequentRenterPoints(int daysRented) {
		return 1;
	}

}
