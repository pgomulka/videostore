package com.gomul.videostore.movie;

import com.gomul.videostore.MovieType;

public class RegularMovie implements MovieType{

	public double calculateFee(int daysRented) {
		double thisAmount = 2;
		if (daysRented > 2)
			thisAmount += (daysRented - 2) * 1.5;
		return thisAmount;
	}

	public int getFrequentRenterPoints(int daysRented) {
		return 1;
	}

}
