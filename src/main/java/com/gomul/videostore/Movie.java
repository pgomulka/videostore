package com.gomul.videostore;

public class Movie {
	private String title;
	private MovieType movieType;

	public Movie(String title, MovieType movieType) {
		this.title = title;
		this.movieType = movieType;
	}

	public String getTitle() {
		return title;
	}

	public double getFeeFor(int daysRented) {
		return movieType.calculateFee(daysRented);
	}

	public int getFrequentRentalPoints(int daysRented) {
		return movieType.getFrequentRenterPoints(daysRented);
	}

}