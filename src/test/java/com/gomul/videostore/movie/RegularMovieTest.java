package com.gomul.videostore.movie;

import org.fest.assertions.Assertions;
import org.junit.Test;

public class RegularMovieTest {
	RegularMovie movie = new RegularMovie();

	@Test
	public void shouldReturnConstantForShortTimeRentals() {
		// when
		double fee = movie.calculateFee(1);

		// then
		Assertions.assertThat(fee).isEqualTo(2);
	}

	@Test
	public void shouldAddExtraFeeForLongerRentals() {
		// when
		double fee = movie.calculateFee(3);

		// then
		Assertions.assertThat(fee).isEqualTo(3.5);
	}
}
