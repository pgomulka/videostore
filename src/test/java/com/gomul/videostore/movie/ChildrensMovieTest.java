package com.gomul.videostore.movie;

import org.fest.assertions.Assertions;
import org.junit.Test;



public class ChildrensMovieTest {
	ChildrensMovie movie = new ChildrensMovie();
	
	@Test
	public void shouldReturnConstantForShortTimeRentals() {
		// when
		double fee = movie.calculateFee(1);

		// then
		Assertions.assertThat(fee).isEqualTo(1.5);
	}
	
	@Test
	public void shouldAddExtraFeeForLongerRentals() {
		// when
		double fee = movie.calculateFee(4);

		// then
		Assertions.assertThat(fee).isEqualTo(3);
	}
	
	
}
