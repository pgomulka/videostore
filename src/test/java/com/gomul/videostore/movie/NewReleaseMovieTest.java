package com.gomul.videostore.movie;

import org.fest.assertions.Assertions;
import org.junit.Test;

public class NewReleaseMovieTest {
	NewReleaseMovie movie = new NewReleaseMovie();

	@Test
	public void shouldCalculateFee() {
		// when
		double fee = movie.calculateFee(1);

		// then
		Assertions.assertThat(fee).isEqualTo(3);
	}
	@Test
	public void shouldNotAddExtraFrequentRenterPoints() {
		// when
		double fee = movie.getFrequentRenterPoints(1);

		// then
		Assertions.assertThat(fee).isEqualTo(1);
	}
	@Test
	public void shouldAddExtraFrequentRenterPoints() {
		// when
		double fee = movie.getFrequentRenterPoints(2);

		// then
		Assertions.assertThat(fee).isEqualTo(2);
	}
	
}
