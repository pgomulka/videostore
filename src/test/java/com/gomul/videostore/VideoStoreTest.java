package com.gomul.videostore;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.gomul.videostore.movie.ChildrensMovie;
import com.gomul.videostore.movie.NewReleaseMovie;
import com.gomul.videostore.movie.RegularMovie;


public class VideoStoreTest {
	private Customer customer = new Customer("Fred");

	//todo consider refactor assertions and method names
	
	@Test
	public void shouldPrintStatementForSingleRental() {
		// given
		customer.addRental(new Rental(new Movie("The Cell", new NewReleaseMovie()), 3));

		// when
		String statement = customer.statement();
		
		// then
		assertThat(statement.split("\n")).isEqualTo(
				new String[] { "Rental Record for Fred", "\tThe Cell\t9.0", "You owed 9.0", "You earned 2 frequent renter points" });

	}

	@Test
	public void testDualNewReleaseStatement() {
		customer.addRental(new Rental(new Movie("The Cell", new NewReleaseMovie()), 3));
		customer.addRental(new Rental(new Movie("The Tigger Movie",new NewReleaseMovie()), 3));
		assertEquals("Rental Record for Fred\n\tThe Cell\t9.0\n\tThe Tigger Movie\t9.0\nYou owed 18.0\nYou earned 4 frequent renter points\n",
				customer.statement());
	}

	@Test
	public void testSingleChildrensStatement() {
		customer.addRental(new Rental(new Movie("The Tigger Movie", new ChildrensMovie()), 3));
		assertEquals("Rental Record for Fred\n\tThe Tigger Movie\t1.5\nYou owed 1.5\nYou earned 1 frequent renter points\n", customer.statement());
	}

	@Test
	public void testMultipleRegularStatement() {
		customer.addRental(new Rental(new Movie("Plan 9 from Outer Space", new RegularMovie()), 1));
		customer.addRental(new Rental(new Movie("8 1/2", new RegularMovie()), 2));
		customer.addRental(new Rental(new Movie("Eraserhead", new RegularMovie()), 3));

		assertEquals(
				"Rental Record for Fred\n\tPlan 9 from Outer Space\t2.0\n\t8 1/2\t2.0\n\tEraserhead\t3.5\nYou owed 7.5\nYou earned 3 frequent renter points\n",
				customer.statement());
	}

}